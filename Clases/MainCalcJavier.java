
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Es el principal del programa.
 * 
 * @author Jose Tejada
 * @author Javier Anleu
 * @version 1.0
 * @since January 26, 2018
 */
public class MainCalcJavier {

	public static void main(String[] arg) {
		File archivo = null;
		FileReader fr = null;
		BufferedReader br = null;

		try {
			// Apertura del fichero y creacion de BufferedReader para poder
			// hacer una lectura comoda (disponer del metodo readLine()).
			archivo = new File("C:\\Users\\javie\\Documents\\BitBucket\\Estructura de Datos\\HT 2\\datos.txt");
			//archivo = new File(
					//"C:\\Users\\javia\\Desktop\\OneDrive para la Empresa\\UVG\\2do Año\\Primer Semestre 2018\\Algoritmos y Estructuras de Datos\\Hoja de Trabajo 2\\CC2003-HT2\\datos.txt");
			fr = new FileReader(archivo);
			br = new BufferedReader(fr);
			calculadora calculadora = new calculadoraConfi();
			// Lectura del ficheroSi
			String linea;
			String lineaOperacion = "";
			while ((linea = br.readLine()) != null)
				lineaOperacion = lineaOperacion + linea;
			// System.out.println(lineaOperacion);
			// System.out.println(calculadora.getMiStack());
			double result = calculadora.operar(lineaOperacion);
			if (Double.isNaN(result)) {
				System.out.println("La operación no devuelve un resultado válido o no puede llevarse a cabo.");
			} else {
				System.out.println("Resultado: " + calculadora.operar(lineaOperacion));
			}

		} catch (IOException e) {
		} /*
			 * finally { // En el finally cerramos el fichero, para asegurarnos // que se
			 * cierra tanto si todo va bien como si salta // una excepcion. try { if (null
			 * != fr) { fr.close(); } } catch (IOException e2) { } }
			 */
	}

}
