
/**
 * @author Javier Carpio
 * @author Josue David Lopez
 * @param <E> Generic que nos permite determinar el tipo de dato a utilizar.
 * @since 26.01.2018
 */
public interface Stack<E>{

   /**
    * pre:
    * post: item is added to stack
    * Metodo que permite agregar un objeto al stack.
    * @param item Objeto a guardarse en el stack.
    */
   public void push(E item);
   
   /**
    * pre: stack is not empty
    * post: most recently pushed item is removed and returned
    * Metodo que permite tomar el primer objeto en el stack.
    * @return Ultimo objeto ingresado en el stack.
    */
   public E pop();
   
   /**
    * pre: stack is not empty
    * post: top value (next to be popped) is returned
    * Metodo que nos permite observar el primero objeto en el stack.
    * @return 
    */
   public E peek();
   
   /**
    * post: returns true if and only if the stack is empty
    * Metodo que nos permite saber si esta vacio el stack.
    * @return True si el stack esta vacio, de lo contrario, false.
    */
   public boolean empty();
   
   /**
    * post: returns the number of elements in the stack
    * Metodo que nos permite conocer el tamano del stack
    * @return Numero de elementos en el stack.
    */
   public int size();
}