
/**
 * @author Javier Carpio
 * @author Josue David Lopez
 * @since 26.01.2018
 */
public interface calculadora {
    
    /**
     * Metodo de interfaz que recibe una linea de texto para ser operada.
     * @param expresion String, linea de texto que contiene numeros y operaciones.
     * @return El restultado total de la operacion.
     */
    public double operar(String expresion);
}
