/**
 * @author Javier Carpio
 * @author Josue David Lopez
 * @since 26.01.2018
 */
import java.io.BufferedReader; 
import java.io.File; 
import java.io.FileNotFoundException;
import java.io.FileReader; 
import java.io.IOException;
import static java.lang.Double.NaN;

public class HojaTrabajo2 {

    public static void main(String[] args) throws FileNotFoundException{
        
    calculadora calc = new calculadoraConfi();
    
    //File archivo = new File("C:\\Users\\JUMPSTONIK\\Documents\\calculadora\\datos.txt");
    File archivo = new File("C:\\Users\\javie\\Documents\\BitBucket\\Estructura de Datos\\HT 2\\datos.txt");
    String texto = new String();
    BufferedReader entrada;
    entrada = new BufferedReader(new FileReader(archivo));
    
    try{
        
        String linea;
        while(entrada.ready()){
            linea = entrada.readLine();
            texto += linea;
            //System.out.println(linea);
        }
        
    }
    catch (IOException e) { 
        e.printStackTrace(); 
    } 
    finally 
    { 
    try{ 
        entrada.close(); 
    }
        catch(IOException e1){} 
    }
    
    boolean option = true;
        for (int i = 0; i < texto.length(); i++){
            
            if ((texto.codePointAt(i) != 32) && (texto.codePointAt(i) != 255) && (texto.codePointAt(i) != 42) && (texto.codePointAt(i) != 43) && (texto.codePointAt(i) != 45) && (texto.codePointAt(i) != 47) && (texto.codePointAt(i) != 48)  && (texto.codePointAt(i) != 49) && (texto.codePointAt(i) != 50) && (texto.codePointAt(i) != 51) && (texto.codePointAt(i) != 52) && (texto.codePointAt(i) != 53) && (texto.codePointAt(i) != 54) && (texto.codePointAt(i) != 55) && (texto.codePointAt(i) != 56) && (texto.codePointAt(i) != 57)) {
                option = false;
                break;
            }else{
                option = true;
            }
            
        }
        if (option == false) {
            System.out.println("Revisar el txt, porque existe algo que no es operando o numero.");
        }
        if (option == true) {
            double resultado = calc.operar(texto);
            if(Double.isNaN(resultado) || (Double.isInfinite(resultado))){
                System.out.println("Revisar el txt, porque ocurre una division entre 0.");
                
            }else{
                System.out.println("El resultado es:\t" + resultado + ".");
            }
            
        }
    
    
    }
    
}
