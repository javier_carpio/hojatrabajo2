import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Javier Carpio
 * @author Josue David Lopez
 * @since 30.01.2018
 */
public class calculadoraConfiTest {
    
    public calculadoraConfiTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of operar method, of class calculadoraConfi.
     */
    @Test
    public void testOperar1() {
        System.out.println("operar");
        String expresion = "1 2 +";
        calculadoraConfi instance = new calculadoraConfi();
        double expResult = 3.0;
        double result = instance.operar(expresion);
        
        assertEquals(expResult, result, 0.0);
    }
    /**
     * Test of operar method, of class calculadoraConfi.
     */
    @Test
    public void testOperar2() {
        System.out.println("operar");
        String expresion = "1 2 + 3 /";
        calculadoraConfi instance = new calculadoraConfi();
        double expResult = 1.0;
        double result = instance.operar(expresion);
        
        assertEquals(expResult, result, 0.0);
    }
    /**
     * Test of operar method, of class calculadoraConfi.
     */
    @Test
    public void testOperar3() {
        System.out.println("operar");
        String expresion = "1 2 + 5 * 3 -";
        calculadoraConfi instance = new calculadoraConfi();
        double expResult = -12.0;
        double result = instance.operar(expresion);
        
        assertEquals(expResult, result, 0.0);
    }
    
}
