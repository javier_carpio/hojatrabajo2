
import static java.lang.Double.NaN;


/**
 * @author Javier Carpio
 * @author Josue David Lopez
 * @since 26.01.2018
 */
public class calculadoraConfi implements calculadora{
    
    Stack<Double> stack;
    
    /**
     * Constructor que nos permite crear un objeto de la clase Stack.
     */
    calculadoraConfi(){
        stack = new StackArrayList<>();
    }
    
    /**
     * Metodo que nos permite realizar la operacion correspondiente.
     * @param expresion String con la operacion a realizar.
     * @return Resultado de la operacion.
     */
    @Override
    public double operar(String expresion) {
        boolean permiso = false;
        String operacion = "";
        
        for(int a = 0; a < expresion.length(); a++){
            if(permiso == false){
                operacion = expresion.substring(4, 5);
                permiso = true;
                
                stack.push(Double.parseDouble(expresion.substring(0, 1)));
                expresion = expresion.substring(a + 2);
                stack.push(Double.parseDouble(expresion.substring(0, 1)));
                expresion = expresion.substring(3);
                
                elegirOperacion(operacion);
                
            }else if(permiso == true){
                String dato = expresion.substring(a, a + 1);
                elegirOperacion(dato);
            }
        }
        
        return stack.peek();
    }
    
    /**
     * Metodo uqe nos permite seleccionar el tipo de operacion a realizar.
     * @param dato Simbolo para reconocer tipo de operacion.
     */
    private void elegirOperacion(String dato){
        switch (dato) {
            case "+":
                stack.push(Suma(stack.pop(), stack.pop()));
                break;
            case "-":
                stack.push(Resta(stack.pop(), stack.pop()));
                break;
            case "*":
                stack.push(Multiplicacion(stack.pop(), stack.pop()));
                break;
            case "/":
                stack.push(Division(stack.pop(), stack.pop()));
                break;
            case " ":
                break;
            default:
                stack.push(Double.parseDouble(dato));
                break;
        }
    }
    
    /**
     * Metodo que nos realiza la suma de dos numeros.
     * @param num1 Primero numero ingresado al Stack.
     * @param num2 Segundo numero ingresado al Stack.
     * @return Suma de numeros.
     */
    private double Suma(double num1, double num2){
        return (num1 + num2);
    }
    
    /**
     * Metodo que nos realiza la resta de dos numeros.
     * @param num1 Primero numero ingresado al Stack.
     * @param num2 Segundo numero ingresado al Stack.
     * @return Resta de numeros.
     */
    private double Resta(double num1, double num2){
        return (num1 - num2);
    }
    
    /**
     * Metodo que nos realiza la multiplicacion de dos numeros.
     * @param num1 Primero numero ingresado al Stack.
     * @param num2 Segundo numero ingresado al Stack.
     * @return Multiplicacion de dos numeros
     */
    private double Multiplicacion(double num1, double num2){
        return (num1*num2);
    }
    
    /**
     * Metodo que nos realiza la division de dos numeros.
     * @param num1 Primero numero ingresado al Stack.
     * @param num2 Segundo numero ingresado al Stack.
     * @return Division de dos numeros.
     */
    private double Division(double num1, double num2){
        double var;
        try{
            var = num1/num2;
        }catch(Exception e){
            var = NaN;
        }
        
        return (var);
    }
    
}
