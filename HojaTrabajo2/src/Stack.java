
/**
 * @author Javier Carpio
 * @author Josue David Lopez
 * @param <E> Generic que nos permite determinar el tipo de dato a utilizar.
 * @since 26.01.2018
 */
public interface Stack<E>{

   /**
    * Metodo que permite agregar un objeto al stack.
    * @param item Objeto a guardarse en el stack.
    */
   public void push(E item);
   
   /**
    * Metodo que permite tomar el primer objeto en el stack.
    * @return Ultimo objeto ingresado en el stack.
    */
   public E pop();
   
   /**
    * Metodo que nos permite observar el primero objeto en el stack.
    * @return 
    */
   public E peek();
   
   /**
    * Metodo que nos permite saber si esta vacio el stack.
    * @return True si el stack esta vacio, de lo contrario, false.
    */
   public boolean empty();
   
   /**
    * Metodo que nos permite conocer el tamano del stack
    * @return Numero de elementos en el stack.
    */
   public int size();
}