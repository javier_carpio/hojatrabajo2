import java.util.ArrayList;

/**
 * @author Javier Carpio
 * @author Josue David Lopez
 * @param <E> Generic que nos permite determinar el tipo de dato a utilizar.
 * @since 26.01.2018
 */
public class StackArrayList<E> implements Stack<E>
{
	protected ArrayList<E> datos;
        
        /**
         * Constructor que crea el atributo de ArrayList.
         */
	public StackArrayList(){
            datos = new ArrayList<>();
	}

        /**
         * Metodo que nos permite ingresar un objeto al Stack.
         * @param item Objeto que se ingresa al stack.
         */
        @Override
	public void push(E item){
            datos.add(item);
	}

        /**
         * Metodo que nos permite quitar un objeto del stack.
         * @return Ultimo objeto ingresado.
         */
        @Override
	public E pop(){
            return datos.remove(size()-1);
	}

        /**
         * Metodo que nos permite ver cual objeto se encuentra hasta arriba del stack.
         * @return Ultimo objeto ingresado.
         */
        @Override
	public E peek(){
            return datos.get(size() - 1);
	}
	
        /**
         * Metodo que nosp permite conocer la longitud del stack.
         * @return Tamano del Stack.
         */
        @Override
	public int size(){
            return datos.size();
	}
  
        /**
         * Metodo que nos permite saber si el stack se encuentra vacio.
         * @return Devuelve True si el stack esta vacio.
         */
        @Override
	public boolean empty(){
            return size() == 0;
	}
}